<?php
/**
 * @package WordPress
 * @subpackage Imperio
 */

	add_action( 'after_setup_theme', 'imperio_setup' );
	
	function imperio_setup(){
		
		//remove notifications
		add_action( 'vc_before_init', 'imperio_vcSetAsTheme' );
		function imperio_vcSetAsTheme() {
		    vc_set_as_theme(true);
			update_option('wpb_js_gutenberg_disable',1);
		}
		if (function_exists( 'set_revslider_as_theme' )){
			add_action( 'init', 'imperio_set_revslider_as_theme' );
			function imperio_set_revslider_as_theme() {
				set_revslider_as_theme();
			}
		}
	
		add_theme_support( 'automatic-feed-links' );
		
		/* Add theme-supported features. */
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
		/* new gutenberg features */
		add_theme_support( 'align-wide' );
		
		add_editor_style("/css/layout-style.css");
		
		/* Add custom actions. */
		/**
		 * Make theme available for translation
		 * Translations can be filed in the /languages/ directory
		 */
		load_theme_textdomain( 'imperio', get_template_directory() . '/languages' );
			
		$locale = get_locale();
		$locale_file = get_template_directory() . "/languages/$locale.php";
		if ( is_readable( $locale_file ) )
			require_once( $locale_file );
		/*
	
		/**
		 * Set the content width based on the theme's design and stylesheet.
		 */
		if ( ! isset( $content_width ) )
			$content_width = 900;
		
		//WMPL
		/**
		 * register panel strings for translation
		 */
		if (function_exists ( 'icl_register_string' )){
			require_once (get_template_directory().'/inc/theme-wpml.php');
		}
		//\WMPL
		
		//declare some global variables that will be used everywhere
		global $imperio_new_meta_boxes,
		$imperio_new_meta_post_boxes,
		$imperio_new_meta_portfolio_boxes,
		$imperio_buttons,
		$imperio_data;
		$imperio_new_meta_boxes=array();
		$imperio_new_meta_post_boxes=array();
		$imperio_new_meta_portfolio_boxes=array();
		$imperio_buttons=array();
		$imperio_data=new stdClass();
		
		
		/*----------------------------------------------------------------
		 *  DEFINE THE MAIN CONSTANTS
		 *---------------------------------------------------------------*/
		//main theme info constants
		
		$my_theme = wp_get_theme();
		define("IMPERIO_VERSION", $my_theme->Version);
		//define the main paths and URLs
		define("IMPERIO_LIB_PATH", get_template_directory() . '/lib/');
		define("IMPERIO_LIB_URL", get_template_directory_uri().'/lib/');
		define("IMPERIO_JS_PATH", get_template_directory_uri().'/js/');
		define("IMPERIO_CSS_PATH", get_template_directory_uri().'/css/');
	
		define("IMPERIO_FUNCTIONS_PATH", IMPERIO_LIB_PATH . 'functions/');
		define("IMPERIO_FUNCTIONS_URL", IMPERIO_LIB_URL.'functions/');
		define("IMPERIO_CLASSES_PATH", IMPERIO_LIB_PATH.'classes/');
		define("IMPERIO_OPTIONS_PATH", IMPERIO_LIB_PATH.'options/');
		define("IMPERIO_WIDGETS_PATH", IMPERIO_LIB_PATH.'widgets/');
		define("IMPERIO_SHORTCODES_PATH", IMPERIO_LIB_PATH.'shortcodes/');
		define("IMPERIO_PLUGINS_PATH", IMPERIO_LIB_PATH.'plugins/');
		define("IMPERIO_UTILS_URL", IMPERIO_LIB_URL.'utils/');
		
		define("IMPERIO_IMAGES_URL", IMPERIO_LIB_URL.'images/');
		define("IMPERIO_CSS_URL", IMPERIO_LIB_URL.'css/');
		define("IMPERIO_SCRIPT_URL", IMPERIO_LIB_URL.'script/');
		define("IMPERIO_PATTERNS_URL", get_template_directory_uri().'/images/imperio_patterns/');
		$uploadsdir=wp_upload_dir();
		define("IMPERIO_UPLOADS_URL", $uploadsdir['url']);
		define("IMPERIO_SEPARATOR", '|*|');
		define("IMPERIO_OPTIONS_PAGE", 'imperio_options');
		define("IMPERIO_STYLE_OPTIONS_PAGE", 'imperio_style_options');
		define("IMPERIO_DEMOS_PAGE", 'imperio_demos');
	
		/*----------------------------------------------------------------
		 *  INCLUDE THE FUNCTIONS FILES
		 *---------------------------------------------------------------*/
				
		require_once (IMPERIO_FUNCTIONS_PATH.'general.php');  //some main common functions
		require_once (IMPERIO_FUNCTIONS_PATH.'stylesheet.php');  //some main common functions
		add_action('wp_enqueue_scripts', 'imperio_style', 1);
		add_action('wp_enqueue_scripts','imperio_custom_head', 2);
		add_action('wp_enqueue_scripts', 'imperio_scripts', 10);
	
		
		require_once (IMPERIO_FUNCTIONS_PATH.'sidebars.php');  //the sidebar functionality
		if ( isset($_GET['page']) && $_GET['page'] == IMPERIO_OPTIONS_PAGE ){
			require_once (IMPERIO_CLASSES_PATH.'upper-options-manager.php');  //the theme options manager functionality
		}
		if ( isset($_GET['page']) && $_GET['page'] == IMPERIO_STYLE_OPTIONS_PAGE ){
			require_once (IMPERIO_CLASSES_PATH.'upper-style-options-manager.php');  //the theme options manager functionality
		}
		if ( isset($_GET['page']) && $_GET['page'] == IMPERIO_DEMOS_PAGE ){
			require_once (IMPERIO_CLASSES_PATH.'upper-demos-manager.php');  //the theme options manager functionality
		}
			
		require_once (IMPERIO_CLASSES_PATH.'upper-templater.php');  
		require_once (IMPERIO_CLASSES_PATH.'upper-custom-data-manager.php');  
		require_once (IMPERIO_CLASSES_PATH.'upper-custom-page.php');  
		require_once (IMPERIO_CLASSES_PATH.'upper-custom-page-manager.php');  
		require_once (IMPERIO_FUNCTIONS_PATH.'custom-pages.php');  //the comments functionality
		require_once (IMPERIO_FUNCTIONS_PATH.'comments.php');  //the comments functionality
		
		do_action( 'imperio_plugin_widgets_init' ); // the imperio-plugin-widgets functionality
		
		require_once (IMPERIO_FUNCTIONS_PATH.'options.php');  //the theme options functionality
		
		if (is_admin()){
			require_once (IMPERIO_FUNCTIONS_PATH. 'meta.php');  //adds the custom meta fields to the posts and pages
			add_action('admin_enqueue_scripts','imperio_admin_style');
		}
		$functions_path = get_template_directory() . '/functions/';
		
		add_filter('add_to_cart_fragments' , 'imperio_woocommerce_header_add_to_cart_fragment' );
		
		// Declare sidebar widget zone
		if (function_exists('register_sidebar')) {
			register_sidebar(array(
				'name' => esc_html__( 'Blog Sidebar', 'imperio' ),
				'id'   => 'sidebar-widgets',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2>',
				'after_title'   => '</h2>'
			));
		}
		
		if (!function_exists('imperio_wp_pagenavi')){ 
			$including = $functions_path. 'wp-pagenavi.php';
		    require_once($including);
		}
		
		/* ------------------------------------------------------------------------ */
		/* Misc
		/* ------------------------------------------------------------------------ */
		// Post Thumbnail Sizes
		if ( function_exists( 'add_image_size' ) ) add_theme_support( 'post-thumbnails' );
		
		if ( function_exists( 'add_image_size' ) ) {
			add_image_size( 'imperio_blog', 1000, 563, true );				// Standard Blog Image
			add_image_size( 'imperio_mini', 80, 80, true ); 				// used for widget thumbnail
			add_image_size( 'imperio_portfolio', 600, 400, true );			// also for blog-medium
			add_image_size( 'imperio_regular', 500, 500, true ); 
			add_image_size( 'imperio_wide', 1000, 500, true ); 
			add_image_size( 'imperio_tall', 500, 1000, true );
			add_image_size( 'imperio_widetall', 1000, 1000, true ); 
		}
		
		/* tgm plugin activator */
		/**
		 * Include the TGM_Plugin_Activation class.
		 */
		require_once get_template_directory() . '/lib/functions/class-tgm-plugin-activation.php';
		
		add_action( 'tgmpa_register', 'imperio_register_required_plugins' );	
		
		if ( class_exists('VCExtendAddonClass')){
			// Finally initialize code
			new VCExtendAddonClass();
		}
		
		if (get_option("imperio_enable_smooth_scroll") == "on"){
			update_option('ultimate_smooth_scroll','enable');
		} else update_option('ultimate_smooth_scroll','disable');
		add_filter( 'woocommerce_enqueue_styles', '__return_false' );
	}
	
	function imperio_admin_style(){
		wp_enqueue_style('imperio-fa-painel', IMPERIO_CSS_PATH .'font-awesome-painel.min.css');
		wp_enqueue_script( 'imperio-admin', IMPERIO_JS_PATH .'imperio-admin.js', array(), '1',$in_footer = true);
	}
	
	
	
	function imperio_wpml_filter_langs( $languages ) {
		foreach ( $languages as $k => $language ) {                                       
			$lang_code = explode ( '-' , $languages[$k]['language_code'] );
			$languages[$k]['native_name']     = ucfirst( $lang_code[0] );
			$languages[$k]['translated_name'] = ucfirst( $lang_code[0] );
		}	
		return $languages;
	}
	add_filter( 'icl_ls_languages', 'imperio_wpml_filter_langs' );
	add_filter('wpml_add_language_selector', 'imperio_wpml_filter_langs');
	

	/*-----------------------------------------------------------------------------------*/
	/*  THEME REQUIRES
	/*-----------------------------------------------------------------------------------*/
	require_once (get_template_directory().'/inc/theme-styles.php');
	
	
	function imperio_custom_head(){
		wp_enqueue_script('imperio-html5trunk', get_template_directory_uri().'/js/html5.js', '1');
		wp_script_add_data( 'imperio-html5trunk', 'conditional', 'lt IE 9' );
	}
	
	function imperio_style() {
	  	wp_enqueue_style('imperio_js_composer_front');
		wp_style_add_data( 'imperio_js_composer_front', 'conditional', 'lt IE 9' );
		
		$theme = wp_get_theme();
		wp_enqueue_style( 'imperio-style', get_template_directory_uri().'/style.css', array(), $theme->Version );
	}
	
	function imperio_slug_post_classes( $classes, $class, $post_id ) {
		$imperio_is_portfolio = array_search( 'type-portfolio', $classes );
		if ( is_single( $post_id ) && false !== $imperio_is_portfolio ) {
			$classes[] = 'container';
		}
		if (is_sticky( $post_id )) $classes[] = 'sticky';	 
		return $classes;
	}
	add_filter( 'post_class', 'imperio_slug_post_classes', 10, 3 );
	
	/*-----------------------------------------------------------------------------------*/
	/*  LOAD THEME SCRIPTS
	/*-----------------------------------------------------------------------------------*/
	function imperio_scripts(){
	
		if (!is_admin()){
			global $vc_addons_url, $wp_query, $post;
			
			if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
			
	  	    wp_enqueue_script( 'imperio-upper-modernizr', IMPERIO_JS_PATH .'utils/upper-modernizr.js', array('jquery'),'1.0',$in_footer = true);
			wp_enqueue_script( 'imperio-upper-waypoint', IMPERIO_JS_PATH .'utils/upper-waypoint.js', array('jquery'),'1.0',$in_footer = true);
			wp_enqueue_script( 'imperio-upper-stellar', IMPERIO_JS_PATH .'utils/upper-stellar.js', array('jquery'),'1.0',$in_footer = true);
			wp_enqueue_script( 'imperio-upper-flex', IMPERIO_JS_PATH .'utils/upper-flex.js', array('jquery'),'1.0',$in_footer = true);
			wp_enqueue_script( 'imperio-upper-iso', IMPERIO_JS_PATH .'utils/upper-iso.js', array('jquery'),'1.0',$in_footer = true);
			wp_enqueue_script( 'imperio-upper-qloader', IMPERIO_JS_PATH .'utils/upper-qloader.js', array('jquery'),'1.0',$in_footer = true);
			wp_enqueue_script( 'imperio-upper-tweet', IMPERIO_JS_PATH .'utils/upper-tweet.js', array('jquery'),'1.0',$in_footer = true);
			wp_enqueue_script( 'imperio-upper-bootstrap', IMPERIO_JS_PATH .'utils/upper-bootstrap.js', array('jquery'),'1.0',$in_footer = true);
			wp_enqueue_script( 'imperio-upper-dlmenu', IMPERIO_JS_PATH .'utils/upper-dlmenu.js', array('jquery'),'1.0',$in_footer = true);
			wp_enqueue_script( 'imperio-upper-greyscale', IMPERIO_JS_PATH .'utils/upper-greyscale.js', array('jquery'),'1.0',$in_footer = true);
			wp_enqueue_script( 'imperio-upper-simpleselect', IMPERIO_JS_PATH .'utils/upper-simpleselect.js', array('jquery'),'1.0',$in_footer = true);
	  	    wp_enqueue_script( 'jquery-effects-core', array('jquery') );
	  	    wp_register_script( 'imperio-global', IMPERIO_JS_PATH .'global.js', array('jquery'), '1',$in_footer = true);
			
			if (is_archive() || is_single() || is_search() || is_page_template('blog-template.php') || is_page_template('blog-masonry-template.php') || is_page_template('blog-masonry-grid-template.php') || is_front_page()) {

				$nposts = get_option('posts_per_page'); $imperio_more = 0; $imperio_pag = 0; $max = 0; $orderby=""; $category=""; $nposts = ""; $order = "";
				$imperio_pag = $wp_query->query_vars['paged'];
				if (!is_numeric($imperio_pag)) $imperio_pag = 1;
				
				$imperio_reading_option = get_option('imperio_blog_reading_type');

				switch ($imperio_reading_option){
					case "scrollauto": 
							// Add code to index pages.
							if( !is_singular() ) {	
								if (is_search()){
									$se = get_option("imperio_enable_search_everything");
									$nposts = get_option('posts_per_page');
									$imperio_pag = $wp_query->query_vars['paged'];
									if (!is_numeric($imperio_pag)) $imperio_pag = 1;

									if ($se == "on"){
										$args = array( 'showposts' => get_option('posts_per_page'), 'post_status' => 'publish', 'paged' => $imperio_pag, 's' => esc_html($_GET['s']));
									    $imperio_the_query = new WP_Query( $args );
									    $args2 = array( 'showposts' => -1, 'post_status' => 'publish', 'paged' => $imperio_pag, 's' => esc_html($_GET['s']) );
										$counter = new WP_Query($args2);
									} else {
										$args = array('showposts' => get_option('posts_per_page'),'post_status' => 'publish','paged' => $imperio_pag,'post_type' => 'post','s' => esc_html($_GET['s']));
									    $imperio_the_query = new WP_Query( $args );
									    $args2 = array('showposts' => -1,'post_status' => 'publish','paged' => $imperio_pag,'post_type' => 'post','s' => esc_html($_GET['s']));
										$counter = new WP_Query($args2);
									}
									$max = ceil($counter->post_count / $nposts);
									$imperio_paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;
								} else {
									$max = $wp_query->max_num_pages;
									$imperio_paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;
								}
								$imperio_inline_script = '
									jQuery(document).ready(function($){
										"use strict";
										if (window.imperioOptions.reading_option === "scrollauto" && !jQuery("body").hasClass("single")){ 
											window.imperio_loadingPoint = 0;
											//monitor page scroll to fire up more posts loader
											window.clearInterval(window.imperio_interval);
											window.imperio_interval = setInterval("imperio_monitorScrollTop()", 1000 );
										}
									});
								';
								wp_add_inline_script('imperio-global', $imperio_inline_script, 'after');
							} else {
							    $args = array('showposts' => $nposts,'orderby' => $orderby,'order' => $order,'cat' => $category,'paged' => $imperio_pag,'post_status' => 'publish');
				    		    $imperio_the_query = new WP_Query( $args );
					    		$max = $imperio_the_query->max_num_pages;
					    		$imperio_paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;
					    		$imperio_inline_script = '
									jQuery(document).ready(function($){
										"use strict";
										if (window.imperioOptions.reading_option === "scrollauto" && !jQuery("body").hasClass("single")){ 
											window.imperio_loadingPoint = 0;
											//monitor page scroll to fire up more posts loader
											window.clearInterval(window.imperio_interval);
											window.imperio_interval = setInterval("imperio_monitorScrollTop()", 1000 );
										}
									});
								';
								wp_add_inline_script('imperio-global', $imperio_inline_script, 'after');

				    		}
						break;
					case "scroll": 
							if( !is_singular() ) {	
								if (is_search()){
									$nposts = get_option('posts_per_page');
									$se = get_option("imperio_enable_search_everything");
									if ($se == "on"){
										$args = array('showposts' => get_option('posts_per_page'),'post_status' => 'publish','paged' => $imperio_pag,'s' => esc_html($_GET['s']));
									    $imperio_the_query = new WP_Query( $args );
									    $args2 = array('showposts' => -1,'post_status' => 'publish','paged' => $imperio_pag,'s' => esc_html($_GET['s']));
										$counter = new WP_Query($args2);
									} else {
										$args = array('showposts' => get_option('posts_per_page'),'post_status' => 'publish','paged' => $imperio_pag,'post_type' => 'post','s' => esc_html($_GET['s']));
									    $imperio_the_query = new WP_Query( $args );
									    $args2 = array('showposts' => -1,'post_status' => 'publish','paged' => $imperio_pag,'post_type' => 'post','s' => esc_html($_GET['s']));
										$counter = new WP_Query($args2);
									}
									$max = ceil($counter->post_count / $nposts);
									$imperio_pag = 1;
									$imperio_pag = $wp_query->query_vars['paged'];
									if (!is_numeric($imperio_pag)) $imperio_pag = 1;
								} else {
									$max = $wp_query->max_num_pages;
									$imperio_paged = $imperio_pag;
								}
							} else {
								$orderby = ""; $category = "";
							    $args = array('showposts' => $nposts,'orderby' => $orderby,'order' => $order,'cat' => $category,'post_status' => 'publish');
				    		    $imperio_the_query = new WP_Query( $args );
					    		$max = $imperio_the_query->max_num_pages;
					    		$imperio_pag = 1;
								$imperio_pag = $wp_query->query_vars['paged'];
								if (!is_numeric($imperio_pag)) $imperio_pag = 1;
				    		}
						break;
				}
			} 
			
			/* pass needed options values to JS */
			$imperioOptions = array(
				"templatepath" => esc_url(get_template_directory_uri())."/",
				"homePATH" => ABSPATH,
				"styleColor" => "#".esc_html(get_option("imperio_style_color")),
				"imperio_no_more_posts_text" => function_exists('icl_t') ? sprintf(esc_html__("%s", "imperio"), icl_t( 'imperio', 'No more posts to load.', get_option('imperio_no_more_posts_text'))) : sprintf(esc_html__("%s", "imperio"), get_option('imperio_no_more_posts_text')),
				"imperio_load_more_posts_text" => function_exists('icl_t') ? sprintf(esc_html__("%s", "imperio"), icl_t( 'imperio', 'Load More Posts', get_option('imperio_load_more_posts_text'))) : sprintf(esc_html__("%s", "imperio"), get_option('imperio_load_more_posts_text')),
				"imperio_loading_posts_text" => function_exists('icl_t') ? sprintf(esc_html__("%s", "imperio"), icl_t( 'imperio', 'Loading posts.', get_option('imperio_loading_posts_text'))) : sprintf(esc_html__("%s", "imperio"), get_option('imperio_loading_posts_text')),
				"searcheverything" => get_option("imperio_enable_search_everything"),
				"imperio_header_shrink" => get_option('imperio_fixed_menu') == 'on' && get_option('imperio_header_after_scroll') == 'on' && get_option('imperio_header_shrink_effect') == 'on' ? 'yes' : 'no',
				"imperio_header_after_scroll" => get_option('imperio_fixed_menu') == 'on' && get_option('imperio_header_after_scroll') == 'on' ? 'yes' : 'no',
				"imperio__portfolio_grayscale_effect" => get_option("imperio_enable_portfolio_grayscale"),
				"imperio__instagram_grayscale_effect" => get_option("imperio_enable_instagram_grayscale"),
				"imperio_enable_ajax_search" => get_option("imperio_enable_ajax_search"),
				"imperio_newsletter_input_text" => function_exists('icl_t') ? esc_html(icl_t( 'imperio', 'Enter your email here', get_option('imperio_newsletter_input_text'))) : esc_html(get_option('imperio_newsletter_input_text')),
				"imperio_update_section_titles" => get_option('imperio_update_section_titles'),
				"imperio_wpml_current_lang" => function_exists('icl_t') ? ICL_LANGUAGE_CODE : "",
				"reading_option" => isset($imperio_reading_option) ? $imperio_reading_option : "paged",
				"loader_startPage" => isset($imperio_pag) ? $imperio_pag : 0,
				"loader_maxPages" => isset($max) ? $max : 0,
				"imperio_grayscale_effect" => get_option("imperio_enable_grayscale")
			);
			
			wp_localize_script( 'imperio-global', 'imperioOptions', $imperioOptions );
			wp_enqueue_script( 'imperio-global' );
			add_action( 'wp_footer', 'imperio_set_import_fonts' );
			
	  	    wp_enqueue_script( 'imperio-jquery-twitter', IMPERIO_JS_PATH .'twitter/jquery.tweet.js', array(),'1.0',$in_footer = true);
	  	   
	  		wp_enqueue_script('cubeportfolio-jquery-js',$in_footer = false);
			wp_enqueue_style('cubeportfolio-jquery-css',$in_footer = false);
			
			if (class_exists('Ultimate_VC_Addons')) {
				wp_enqueue_script('ultimate', plugins_url().'/Ultimate_VC_Addons/assets/min-js/ultimate.min.js', array('jquery'),'3.19.0');
				wp_enqueue_style('ultimate', plugins_url().'/Ultimate_VC_Addons/assets/min-css/ultimate.min.css', '3.19.0');
			}

			if (is_single()){
				wp_enqueue_style( 'prettyphoto'); wp_enqueue_script( 'prettyphoto'); 
			}
			if (isset($post->ID)) $template = get_post_meta( $post->ID, '_wp_page_template' ,true );
						
			if (isset($template) && ( $template == 'template-blank.php' || $template == 'template-under-construction.php' || $template == 'template-home.php' ) || is_404()){
				if (class_exists('Ultimate_VC_Addons')) {
					wp_enqueue_script('ultimate', plugins_url().'/Ultimate_VC_Addons/assets/min-js/ultimate.min.js', array('jquery'),'3.19.0');
					wp_enqueue_style('ultimate', plugins_url().'/Ultimate_VC_Addons/assets/min-css/ultimate.min.css','3.19.0');
					wp_enqueue_script('ultimate-script');
					wp_enqueue_script('ultimate-vc-params');
				}
			}
			
			if (isset($template) && ($template == 'one-page-template.php' || $template == 'template-home.php')){
				wp_enqueue_script('googleapis');
			}
			
			if ((isset($template) && ($template == 'blog-masonry-template.php' || $template == 'blog-template.php' || $template == 'blog-masonry-grid-template.php'|| $template == 'blog-template.php')) || is_archive() || is_front_page() || is_search()){
				wp_enqueue_script( 'imperio-blog', IMPERIO_JS_PATH .'blog.js', array('jquery'), '1',$in_footer = true);
			}
			
			wp_dequeue_style( 'wp-mediaelement' );
			wp_dequeue_script( 'wp-mediaelement' ); 
		}
	}


	/*-----------------------------------------------------------------------------------*/
	/*  FUNCTION FOR INSTALL AND REGISTER THEME PLUGINS
	/*-----------------------------------------------------------------------------------*/
	function imperio_register_required_plugins() {
	
		$plugins = array(
	
			// This is an example of how to include a plugin pre-packaged with a theme
			
			array(
				'name'      => 'Contact Form 7',
				'slug'      => 'contact-form-7',
				'required'  => true,
			),
			array(
				'name'      => 'Widget Importer & Exporter',
				'slug'      => 'widget-importer-exporter',
				'required'  => false,
			),
			array(
				'name'      => 'Really Simple CAPTCHA',
				'slug'      => 'really-simple-captcha',
				'required'  => false,
			),
			array(
				'name'          => 'WPBakery Visual Composer',
				'slug'          => 'js_composer',
				'source'        => 'http://demos.upperthemes.com/plugins/imperio/js_composer.zip',
				'required'      => true,
				'version'       => '6.0.5'
			),
			array(
				'name'      	=> 'Revolution Slider',
				'slug'     	 	=> 'revslider',
				'source'        => 'http://demos.upperthemes.com/plugins/imperio/revslider.zip',
				'required'  	=> true,
				'version'       => '6.1.0'
			),
			array(
				'name'          => 'Ultimate Addons for Visual Composer',
				'slug'          => 'Ultimate_VC_Addons',
				'source'        => 'http://demos.upperthemes.com/plugins/imperio/Ultimate_VC_Addons.zip',
				'required'      => true,
				'version'       => '3.19.0'
			),
			
			array(
				'name'      	=> 'imperio_custom_post_types',
				'slug'     	 	=> 'imperio_custom_post_types',
				'source'        => 'http://demos.upperthemes.com/plugins/imperio/imperio_custom_post_types.zip',
				'required'  	=> true,
				'version'       => '2.4.1'
			),
			array(
				'name'          => 'Cube Portfolio',
				'slug'          => 'cubeportfolio',
				'source'        => 'http://demos.upperthemes.com/plugins/imperio/cubeportfolio.zip',
				'required'      => true,
				'version'       => '3.8.1'
			),
			array(
				'name'      	=> 'Envato Market',
				'slug'     	 	=> 'envato-market',
				'source'        => 'http://demos.upperthemes.com/plugins/imperio/envato-market.zip',
				'required'  	=> true,
				'version'       => '2.0.1'
			),
			
			array(
				'name'      	=> 'Master Slider',
				'slug'     	 	=> 'masterslider',
				'source'        => 'http://demos.upperthemes.com/plugins/imperio/masterslider.zip',
				'required'  	=> true,
				'version'       => '3.2.14'
			)
			
			
		);
	
		// Change this to your theme text domain, used for internationalising strings
		$config = array(
			'domain'       		=> 'imperio',         	// Text domain - likely want to be the same as your theme.
			'default_path' 		=> '',
			'parent_slug'  => 'themes.php',            			// Parent menu slug.
			'menu'         		=> 'install-required-plugins', 	// Menu slug
			'has_notices'      	=> true,                       	// Show admin notices or not
			'is_automatic'    	=> false,					   	// Automatically activate plugins after installation or not
			'message' 			=> '',							// Message to output right before the plugins table
			'strings'      		=> array(
				'page_title'                       			=> esc_html__( 'Install Required Plugins', 'imperio' ),
				'menu_title'                       			=> esc_html__( 'Install Plugins', 'imperio' ),
				'installing'                       			=> esc_html__( 'Installing Plugin: %s', 'imperio' ), // %1$s = plugin name
				'oops'                             			=> esc_html__( 'Something went wrong with the plugin API.', 'imperio' ),
				'notice_can_install_required'     			=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'imperio' ), // %1$s = plugin name(s)
			'notice_can_install_recommended'			=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'imperio' ), // %1$s = plugin name(s)
			'notice_cannot_install'  					=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'imperio' ), // %1$s = plugin name(s)
			'notice_can_activate_required'    			=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'imperio' ), // %1$s = plugin name(s)
			'notice_can_activate_recommended'			=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'imperio' ), // %1$s = plugin name(s)
			'notice_cannot_activate' 					=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'imperio' ), // %1$s = plugin name(s)
			'notice_ask_to_update' 						=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'imperio' ), // %1$s = plugin name(s)
			'notice_cannot_update' 						=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'imperio' ), // %1$s = plugin name(s)
				'install_link' 					  			=> _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'imperio' ),
				'activate_link' 				  			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'imperio' ),
				'return'                           			=> esc_html__( 'Return to Required Plugins Installer', 'imperio' ),
				'plugin_activated'                 			=> esc_html__( 'Plugin activated successfully.', 'imperio' ),
				'complete' 									=> esc_html__( 'All plugins installed and activated successfully. %s', 'imperio' ), // %1$s = dashboard link
				'nag_type'									=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
			)
		);
	
		tgmpa( $plugins, $config );
	
	}
	

	
	/*-----------------------------------------------------------------------------------*/
	/*  THEME REQUIRES
	/*-----------------------------------------------------------------------------------*/
 	if (file_exists(get_stylesheet_directory().'/inc/theme-intro.php')) require_once (get_stylesheet_directory().'/inc/theme-intro.php');
 	else require_once (get_template_directory().'/inc/theme-intro.php');
 	
 	if (file_exists(get_stylesheet_directory().'/inc/theme-header.php')) require_once (get_stylesheet_directory().'/inc/theme-header.php');
 	else require_once (get_template_directory().'/inc/theme-header.php');
 	
 	if (file_exists(get_stylesheet_directory().'/inc/theme-walker-menu.php')) require_once (get_stylesheet_directory().'/inc/theme-walker-menu.php');
 	else require_once (get_template_directory().'/inc/theme-walker-menu.php');
 	
 	if (file_exists(get_stylesheet_directory().'/inc/theme-homeslider.php')) require_once (get_stylesheet_directory().'/inc/theme-homeslider.php');
 	else require_once (get_template_directory().'/inc/theme-homeslider.php');
 	
 	if (file_exists(get_stylesheet_directory().'/inc/theme-breadcrumb.php')) require_once (get_stylesheet_directory().'/inc/theme-breadcrumb.php');
 	else require_once (get_template_directory().'/inc/theme-breadcrumb.php');
 	
 	if (file_exists(get_stylesheet_directory().'/inc/theme-menu.php')) require_once (get_stylesheet_directory().'/inc/theme-menu.php');
 	else require_once (get_template_directory().'/inc/theme-menu.php');
 	
 	if (file_exists(get_stylesheet_directory().'/inc/theme-woocart.php')) require_once (get_stylesheet_directory().'/inc/theme-woocart.php');
 	else require_once (get_template_directory().'/inc/theme-woocart.php');
 	
	
	/*-----------------------------------------------------------------------------------*/
	/*  FUNCTION FOR ONE CLICK FEATURE
	/*-----------------------------------------------------------------------------------*/
	function imperio_autoimport($url, $demo) {
		
		$os = ((strpos(strtolower(PHP_OS), 'win') === 0) || (strpos(strtolower(PHP_OS), 'cygwin') !== false)) ? 'win' : 'other';
		if (!function_exists('WP_Filesystem')){
			$abspath = ($os === "win") ? "\wp-admin\includes\file.php" : "/wp-admin/includes/file.php";
			require_once(ABSPATH.$abspath);
		}
		WP_Filesystem();
		global $wpdb, $wp_filesystem;
		
	    // get the file
	    require_once get_template_directory() . '/lib/classes/upper-content-import.php';
	
	    if ( ! class_exists( 'imperio_Auto_Importer' ) )
	        die( 'imperio_Auto_Importer not found' );
	
	    // call the function
		$upload_dir = wp_upload_dir();
		$demo_file = $url.$demo."/contents.xml";
		$tempfile = $upload_dir['basedir'] . '/temp.xml' ;
		$data = $wp_filesystem->get_contents($demo_file);
		if (!$data) $data = wp_remote_fopen($demo_file);
		$result = $wp_filesystem->put_contents($tempfile, $data, FS_CHMOD_FILE);
		
		if ($result){
			$args = array(
	            'file'        => $tempfile,
	            'map_user_id' => 0
	        );
	        imperio_auto_import( $args );
		}
	
	}


	/*-----------------------------------------------------------------------------------*/
	/*  HEX TO RGB
	/*-----------------------------------------------------------------------------------*/
	function imperio_hex2rgb($hex = "000000") {
		if (is_array($hex)) $hex = "000000";
		$hex = str_replace("#", "", $hex);
		
		if(strlen($hex) == 3) {
			$r = hexdec(substr($hex,0,1).substr($hex,0,1));
			$g = hexdec(substr($hex,1,1).substr($hex,1,1));
			$b = hexdec(substr($hex,2,1).substr($hex,2,1));
		} else {
			$r = hexdec(substr($hex,0,2));
			$g = hexdec(substr($hex,2,2));
			$b = hexdec(substr($hex,4,2));
		}
		$rgb = array($r, $g, $b);
		//return implode(",", $rgb); // returns the rgb values separated by commas
		return $rgb; // returns an array with the rgb values
	}



	function imperio_get_string_between($string, $start, $end){
	    $string = " ".$string;
	    $ini = strpos($string,$start);
	    if ($ini == 0) return "";
	    $ini += strlen($start);
	    $len = strpos($string,$end,$ini) - $ini;
	    return substr($string,$ini,$len);
	}
	
	/* Remove VC Modules */
	if (function_exists('vc_remove_element')){
		vc_remove_element('vc_carousel');
		vc_remove_element('vc_posts_slider');
		vc_remove_element('vc_gallery');
		vc_remove_element('vc_images_carousel');
		vc_remove_element('vc_button');
		vc_remove_element('vc_cta_button');
	}
	
	
	/*-----------------------------------------------------------------------------------*/
	/*  INCLUDE ADDONS IN IMPERIO THEME
	/*-----------------------------------------------------------------------------------*/
	function imperio_content_shortcoder($post_content, $loadglobally = false){
		
		$dependancy = array('jquery');
		global $vc_addons_url;

			
		if (isset($vc_addons_url) && $vc_addons_url != ""){
			
			$js_path = 'assets/min-js/';
			$css_path = 'assets/min-css/';
			$ext = '.min';
			$isAjax = true;
			$ultimate_smooth_scroll = get_option('ultimate_smooth_scroll');
	
			// register js
			wp_register_script('ultimate-script',$vc_addons_url.'assets/min-js/ultimate.min.js',array('jquery', 'jquery-ui-core' ), '3.19.0', false);
			wp_register_script('ultimate-appear',$vc_addons_url.$js_path.'jquery-appear'.$ext.'.js',array('jquery'), '3.19.0');
			wp_register_script('ultimate-custom',$vc_addons_url.$js_path.'custom'.$ext.'.js',array('jquery'), '3.19.0');
			wp_register_script('ultimate-vc-params',$vc_addons_url.$js_path.'ultimate-params'.$ext.'.js',array('jquery'), '3.19.0');
			if($ultimate_smooth_scroll === 'enable') {
				$smoothScroll = 'SmoothScroll-compatible.min.js';
			}
			else {
				$smoothScroll = 'SmoothScroll.min.js';
			}
			wp_register_script('ultimate-smooth-scroll',$vc_addons_url.'assets/min-js/'.$smoothScroll,array('jquery'),'3.19.0',true);
			wp_register_script("ultimate-modernizr",$vc_addons_url.$js_path.'modernizr-custom'.$ext.'.js',array('jquery'),'3.19.0');
			wp_register_script("ultimate-tooltip",$vc_addons_url.$js_path.'tooltip'.$ext.'.js',array('jquery'),'3.19.0');
	
			// register css
			wp_register_style('ultimate-animate',$vc_addons_url.$css_path.'animate'.$ext.'.css',array(),'3.19.0');
			wp_register_style('ultimate-style',$vc_addons_url.$css_path.'style'.$ext.'.css',array(),'3.19.0');
			wp_register_style('ultimate-style-min',$vc_addons_url.'assets/min-css/ultimate.min.css',array(),'3.19.0');
			wp_register_style('ultimate-tooltip',$vc_addons_url.$css_path.'tooltip'.$ext.'.css',array(),'3.19.0');
	
			$ultimate_smooth_scroll = get_option('ultimate_smooth_scroll');
			if($ultimate_smooth_scroll == "enable" || $ultimate_smooth_scroll === 'enable') {
				wp_enqueue_script('ultimate-smooth-scroll');
			}
	
			if(function_exists('vc_is_editor')){
				if(vc_is_editor()){
					wp_enqueue_style('vc-fronteditor',$vc_addons_url.'assets/min-css/vc-fronteditor.min.css');
				}
			}
	
			$ultimate_global_scripts = ($loadglobally) ? 'enable' : bsf_get_option('ultimate_global_scripts');

			if($ultimate_global_scripts === 'enable') {
				
				wp_enqueue_script('ultimate-modernizr');
				wp_enqueue_script('jquery_ui');
				wp_enqueue_script('masonry');
				if(defined('DISABLE_ULTIMATE_GOOGLE_MAP_API') && (DISABLE_ULTIMATE_GOOGLE_MAP_API == true || DISABLE_ULTIMATE_GOOGLE_MAP_API == 'true'))
					$load_map_api = false;
				else
					$load_map_api = true;
				if($load_map_api)
					wp_enqueue_script('googleapis');
				wp_enqueue_script('ultimate-script');
				wp_enqueue_script('ultimate-modal-all');
				wp_enqueue_script('jquery.shake',$vc_addons_url.$js_path.'jparallax'.$ext.'.js');
				wp_enqueue_script('jquery.vhparallax',$vc_addons_url.$js_path.'vhparallax'.$ext.'.js');
	
				wp_enqueue_style('ultimate-style-min');
				wp_enqueue_style("ult-icons");
				wp_enqueue_style('ultimate-vidcons',$vc_addons_url.'assets/fonts/vidcons.css');
				wp_enqueue_script('jquery.ytplayer',$vc_addons_url.$js_path.'mb-YTPlayer'.$ext.'.js');
	
				$Ultimate_Google_Font_Manager = new Ultimate_Google_Font_Manager;
				$Ultimate_Google_Font_Manager->enqueue_selected_ultimate_google_fonts();
	
				return false;
			}
	
			if(!is_404() && !is_search()){
	
				if(stripos($post_content, 'font_call:'))
				{
					preg_match_all('/font_call:(.*?)"/',$post_content, $display);
					if (function_exists('enquque_ultimate_google_fonts_optimzed')) enquque_ultimate_google_fonts_optimzed($display[1]);
				}
				
				if( stripos( $post_content, '[swatch_container') || 
				    stripos( $post_content, '[ultimate_modal'))
				{
					wp_enqueue_script('ultimate-modernizr');
				}

				if( stripos( $post_content, '[ultimate_exp_section') ||
					stripos( $post_content, '[info_circle') ) {
					wp_enqueue_script('jquery_ui');
				}

				if( stripos( $post_content, '[icon_timeline') ) {
					wp_enqueue_script('masonry');
				}

				if($isAjax == true) { // if ajax site load all js
					wp_enqueue_script('masonry');
				}

				if( stripos( $post_content, '[ultimate_google_map') ) {
					if(defined('DISABLE_ULTIMATE_GOOGLE_MAP_API') && (DISABLE_ULTIMATE_GOOGLE_MAP_API == true || DISABLE_ULTIMATE_GOOGLE_MAP_API == 'true'))
						$load_map_api = false;
					else
						$load_map_api = true;
					if($load_map_api)
						wp_enqueue_script('googleapis');
				}

				if( stripos( $post_content, '[ult_range_slider') ) {
					wp_enqueue_script('jquery-ui-mouse');
					wp_enqueue_script('jquery-ui-widget');
					wp_enqueue_script('jquery-ui-slider');
					wp_enqueue_script('ult_range_tick');
					wp_enqueue_script('ult_ui_touch_punch');
				}

				wp_enqueue_script('ultimate-script');

				if( stripos( $post_content, '[ultimate_modal') ) {
					wp_enqueue_script('ultimate-modal-all');
				}
				
				$ultimate_css = "enable";
	
				if ($ultimate_css == "enable"){
					wp_enqueue_style('ultimate-style-min');
					if( stripos( $post_content, '[ultimate_carousel') ) {
						wp_enqueue_style("ult-icons");
					}
				} 
			}
		}
	}	

	/*-----------------------------------------------------------------------------------*/
	/*  REQUIRED FOR WOOCOMMERCE CART
	/*-----------------------------------------------------------------------------------*/
	require_once (get_template_directory().'/inc/theme-woocart.php');
	
	
	function imperio_allowed_tags() {
		global $allowedtags, $allowedposttags;
		$allowedtags['option'] = array('style'=>array(), 'id'=>array(), 'name'=>array(), 'class'=>array(), 'value'=>array(), 'selected'=>array());
		$allowedtags['input'] = array('style'=>array(), 'id'=>array(), 'name'=>array(), 'class'=>array(), 'value'=>array(), 'selected'=>array(), 'type'=>array(), 'onchange'=>array(), 'placeholder'=>array());
		$allowedtags['label'] = array('for'=>array());
		$allowedtags['iframe'] = array('style'=>array(), 'src'=>array(), 'allowfullscreen'=>array());
		$allowedposttags['div']['aria-hidden'] = array();
		$allowedposttags['div']['style'] = array();
		$allowedtags = array_merge($allowedtags, $allowedposttags);
	}
	add_action('init', 'imperio_allowed_tags', 10);

	function imperio_get_the_woo(){
		global $woocommerce;
		return isset($woocommerce) ? $woocommerce : array(); 
	}

	/*-----------------------------------------------------------------------------------*/
	/*  LOAD GOOGLE FONTS
	/*-----------------------------------------------------------------------------------*/
	function imperio_fonts_url() {
		global $imperio_import_fonts;
		
		$imperio_import_fonts = imperio_get_import_fonts();
		if (!is_array($imperio_import_fonts) && is_string($imperio_import_fonts)) $imperio_import_fonts = explode("|",$imperio_import_fonts);
		
		$aux = array();
		foreach ($imperio_import_fonts as $font){
			$aux[] = str_replace("|", ":", str_replace(" ", "+", $font));
		}
		
		$aux = array_unique($aux);
		
		$http = (is_ssl( )) ? "https:" : "http:";
		
		$keys = array("Arial","Arial+Black","Helvetica+Neue","Helvetica","Courier+New","Georgia","Impact","Lucida+Sans","Times+New+Roman","Trebuchet+MS","Verdana");
		
		foreach ($keys as $key){
			if (($key_search = array_search($key, $aux)) !== false) {
			    unset($aux[$key_search]);
			}
		}
		
		$imperio_import_fonts = implode("|", $aux);
	    $font_url = '';
	    /*
	    Translators: If there are characters in your language that are not supported
	    by chosen font(s), translate this to 'off'. Do not translate into your own language.
	     */
	    if ( 'off' !== _x( 'on', 'Google font: on or off', 'imperio' ) ) {
	        $font_url = add_query_arg( 'family', $imperio_import_fonts, $http."//fonts.googleapis.com/css", array(), null, 'all' );
	    }
	    return $font_url;
	}
	
	function imperio_google_fonts_scripts() {
	    wp_enqueue_style( 'imperio-google-fonts', imperio_fonts_url(), '' );
	}
	
	function imperio_get_custom_inline_css(){
		global $imperio_inline_css;
		wp_enqueue_style('imperio-custom-style', IMPERIO_CSS_PATH .'imperio-custom.css',99);
		if (!$imperio_inline_css) $imperio_inline_css = "";
		$imperio_inline_css .= "body{visibility:visible;}";
		wp_add_inline_style('imperio-custom-style', $imperio_inline_css);
	}
	
	function imperio_set_custom_inline_css($css){
		global $imperio_inline_css;
		$upper_theme_main_color = "#".get_option('imperio_style_color');
		$imperio_inline_css .= str_replace( '__USE_THEME_MAIN_COLOR__', $upper_theme_main_color, $css );
	}
	
	function imperio_set_team_profiles_content($content){
		global $imperio_team_profiles;
		if (!isset($imperio_team_profiles)) $imperio_team_profiles = '';
		$imperio_team_profiles .= $content;
	}
	
	function imperio_get_team_profiles_content(){
		global $imperio_team_profiles;
		if (isset($imperio_team_profiles)){
			$imperio_team_profiles = wp_kses_no_null( $imperio_team_profiles, array( 'slash_zero' => 'keep' ) );
			$imperio_team_profiles = wp_kses_normalize_entities($imperio_team_profiles);
			echo wp_kses_hook($imperio_team_profiles, 'post', array());
		}
	}
	
	// ajax workers
	//front
	if (get_option('imperio_enable_search')=="on" && get_option('imperio_enable_ajax_search')=="on"){
		include_once get_template_directory() . '/ajaxsearch.php';
	}
	include_once get_template_directory() . '/js/twitter/index.php';
// // 	//back
	if ( is_admin() && current_user_can( 'manage_options' ) ) {
		require_once get_template_directory() . '/lib/functions/queryprojectsforcube.php';
 		require_once get_template_directory() . '/lib/script/loadSettings.php';
		require_once get_template_directory() . '/lib/functions/imperio_demo_installer.php';
		require_once get_template_directory() . '/lib/utils/upload-handler.php';
		$imperio_imported_ids = get_option('imperio_imported_ids');
		if (!$imperio_imported_ids){
			$imperio_imported_ids = array();
			update_option('imperio_imported_ids', $imperio_imported_ids);
		}
		$imperio_imported_ids;
	}

	if (!function_exists('imperio_get_imported_ids')){
		function imperio_get_imported_ids(){
			global $imperio_imported_ids;
			return $imperio_imported_ids;
		}
	}

	if (!function_exists('imperio_set_imported_ids')){
		function imperio_set_imported_ids($ids){
			global $imperio_imported_ids;
			$imperio_imported_ids = $ids;
			update_option('imperio_imported_ids', $imperio_imported_ids);
		}
	}

	if (!function_exists('imperio_add_imported_id')){
		function imperio_add_imported_id( $id, $revs = false ){
			$aux = get_option('imperio_imported_ids');
			$output = "";
			if ($revs){
				if ( !array_search( $id, $aux ) ){
					array_push( $aux, $id );
					imperio_set_imported_ids($aux);
					return true;
				} else {
					return false;
				}
			} else {
				if ( !array_search( intval($id), $aux ) ){
					array_push( $aux, (int)$id );
					imperio_set_imported_ids($aux);
					return true;
				} else {
					return false;
				}
			}
		}
	}