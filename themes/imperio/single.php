<?php
/**
 * @package WordPress
 * @subpackage Imperio
 */

get_header(); imperio_print_menu(); ?>
	
	<?php 
		if (have_posts()) {
			the_post(); 
			$imperio_type = get_post_type();
			$imperio_portfolio_permalink = get_option("imperio_portfolio_permalink");
			switch ($imperio_type){
				case "post":
					get_template_part('post-single', 'single');
				break;
				case $imperio_portfolio_permalink:
					get_template_part('proj-single', 'single');
				break;
				default:
					the_content();
				break;
			}
		}
	?>
	
<?php get_footer(); ?>